package com.formation.genealogie.dao;

import com.formation.genealogie.model.Person;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository("fakeDao")
public class FakePersonDataAccessService implements PersonDao {

    private static List<Person> DB = new ArrayList<>();

    @Override
    public int insertPerson(UUID id, Date currentDate, Person person) {
        DB.add(new Person(id, person.getLastname(), person.getFirstname(), person.getFAMS(), person.getFAMC(),
                currentDate, currentDate));
        return 0;
    }

    @Override
    public List<Person> selectAllPeople() {
        return DB;
    }

    @Override
    public Optional<Person> selectPersonById(UUID id) {
        return DB.stream()
                .filter(person -> person.getId().equals(id))
                .findFirst();
    }

    @Override
    public int deletePersonById(UUID id) {
        Optional<Person> personMaybe = selectPersonById(id);
        if (personMaybe.isEmpty()) {
            return 0;
        } else {
            DB.remove(personMaybe.get());
            return 1;
        }
    }

    @Override
    public int updatePersonById(UUID id, Person personToUpdate) {
        return selectPersonById(id)
                .map(person -> {
                    int indexOfPersonToDelete = DB.indexOf(person);
                    if(indexOfPersonToDelete >= 0) {
                        DB.set(indexOfPersonToDelete, new Person(
                                person.getId(),
                                personToUpdate.getLastname(),
                                personToUpdate.getFirstname(),
                                personToUpdate.getFAMS(),
                                person.getFAMC(),
                                person.getCreation(),
                                new Date()
                                ));
                        return 1;
                    }
                    return 0;
                })
                .orElse(0);
    }
}
