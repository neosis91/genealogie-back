package com.formation.genealogie.dao;

import com.formation.genealogie.model.Person;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PersonDao {

    int insertPerson(UUID id, Date creation,  Person person);

     default int insertPerson(Person person) {
         UUID id = UUID.randomUUID();
         Date currentDate = new Date();
         return insertPerson(id, currentDate, person);
     }

     List<Person> selectAllPeople();
     Optional<Person> selectPersonById(UUID id);
     int deletePersonById(UUID id);
     int updatePersonById(UUID id, Person person);
}
