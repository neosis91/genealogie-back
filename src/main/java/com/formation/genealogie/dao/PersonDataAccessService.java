package com.formation.genealogie.dao;

import com.formation.genealogie.model.Person;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository("postgres")
public class PersonDataAccessService implements PersonDao {

    private final JdbcTemplate jdbcTemplate;

    public PersonDataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    @Override
    public int insertPerson(UUID id, Date creation, Person person) {
        return 0;
    }

    @Override
    public List<Person> selectAllPeople() {
        final String allPersonSql = "SELECT * FROM person";
        return jdbcTemplate.query(allPersonSql, (resultSet, i) ->
                new Person(
                        UUID.fromString(resultSet.getString("id")),
                        resultSet.getString("lastname"),
                        resultSet.getString("firstname"),
                        resultSet.getInt("FAMS"),
                        resultSet.getInt("FAMC"),
                        resultSet.getDate("creation"),
                        resultSet.getDate("update")
                ));
    }

    @Override
    public Optional<Person> selectPersonById(UUID id) {
        final String onePersonSql = "SELECT * FROM person WHERE id = ?";
        Person person = jdbcTemplate.queryForObject(
                onePersonSql,
                new Object[]{id},
                (resultSet, i) -> new Person(
                        UUID.fromString(resultSet.getString("id")),
                        resultSet.getString("lastname"),
                        resultSet.getString("firstname"),
                        resultSet.getInt("FAMS"),
                        resultSet.getInt("FAMC"),
                        resultSet.getDate("creation"),
                        resultSet.getDate("update")
                ));
        return Optional.ofNullable(person);
    }

    @Override
    public int deletePersonById(UUID id) {
        return 0;
    }

    @Override
    public int updatePersonById(UUID id, Person person) {
        return 0;
    }
}
