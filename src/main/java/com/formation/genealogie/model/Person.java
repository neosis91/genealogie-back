package com.formation.genealogie.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.UUID;

public class Person {
    private final UUID id;
    public String lastname;
    public String firstname;
    public Number FAMS;
    public Number FAMC;
    public final Date creation;
    public final Date update;

    public Person(@JsonProperty("id") UUID id,
                  @JsonProperty("lastname") String lastname,
                  @JsonProperty("firstname") String firstname,
                  @JsonProperty("FAMS") Number FAMS,
                  @JsonProperty("FAMC") Number FAMC,
                  @JsonProperty("creation") Date creation,
                  @JsonProperty("update") Date update) {
        this.id = id;
        this.lastname = lastname;
        this.firstname = firstname;
        this.FAMS = FAMS;
        this.FAMC = FAMC;
        this.creation = creation;
        this.update = update;
    }

    public UUID getId() {
        return id;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public Number getFAMS() {
        return FAMS;
    }

    public void setFAMS(Number FAMS) {
        this.FAMS = FAMS;
    }

    public Number getFAMC() {
        return FAMC;
    }

    public void setFAMC(Number FAMC) {
        this.FAMC = FAMC;
    }

    public Date getCreation() {
        return creation;
    }

    public Date getUpdate() {
        return update;
    }
}
