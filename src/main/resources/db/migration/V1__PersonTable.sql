CREATE TABLE person(
    id UUID NOT NULL PRIMARY KEY,
    lastname VARCHAR(100),
    firstname VARCHAR(100),
    FAMS NUMERIC,
    FAMC NUMERIC,
    creation date,
    update date
)
